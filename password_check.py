# Script Name : password_check.py
# Author      : ASSOULINE Jordan
# Created     : July 26th 2022
# Version     : 1.0
# Description : Check if your password is too worst

user_password = input("Veuillez entrer le mot de passe à vérifier : \n")


def worst_500_passwords(password):

    # Download the SecList
    import requests
    print('Beginning check in 500 worst passwords')
    base_url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/'
    head_url = 'master/Passwords/Common-Credentials/500-worst-passwords.txt'
    url = base_url + head_url
    retrieve = requests.get(url)

    content = retrieve.content

    if str(password) in str(content):
        print("Your password is in 500 worst passwords...\
Change it immediately\n")
        exit(0)


def worst_10k_passwords(password):

    # Download the SecList
    import requests
    print('Beginning check in 10K worst passwords')
    base_url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/'
    head_url = 'master/Passwords/Common-Credentials/10k-most-common.txt'
    url = base_url + head_url
    retrieve = requests.get(url)

    content = retrieve.content

    if str(password) in str(content):
        print("Your password is in 10K worst passwords...\
Change it immediately\n")
        exit(0)


if __name__ == "__main__":
    worst_500_passwords(user_password)
    worst_10k_passwords(user_password)
